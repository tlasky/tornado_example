import re
import bcrypt
from typing import *
from pony.orm import *
from config import pony
from datetime import datetime

db = Database()
db.bind(**pony)

hashtag_re = re.compile('(?:^|\s)[＃#]{1}(\w+)', re.UNICODE)


class User(db.Entity):
    username = Required(str, unique=True)
    password_hash = Required(str)

    tasks = Set('Task')

    @staticmethod
    def generate_password_hash(password: str) -> str:
        """
        User(
            username=...,
            password_hash=User.generate_password_hash(...)
        )
        """
        return bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()

    def check_password(self, password: str) -> bool:
        return bcrypt.checkpw(password.encode(), self.password_hash.encode())

    @property
    def tags(self) -> List['TaskTag']:
        return TaskTag.select(
            lambda tag:
            (
                task in Task.select(lambda task: task.owner is self)
                for task in tag.tasks
            )
        ).order_by(TaskTag.name)


class TaskTag(db.Entity):
    name = Required(str, unique=True)
    tasks = Set('Task')

    def __str__(self) -> str:
        return self.name

    @classmethod
    def get_or_create(cls, name: str) -> 'TaskTag':
        return cls.get(name=name) or cls(name=name)


class Task(db.Entity):
    raw_text = Required(str)
    created_at = Required(datetime, default=lambda: datetime.now())
    valid_from = Optional(datetime)
    valid_to = Optional(datetime)
    tags = Set(TaskTag)
    owner = Required(User)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.update()

    @property
    def text(self) -> str:
        return self.raw_text

    @text.setter
    def text(self, value: str) -> None:
        self.raw_text = value
        self.update()

    def update(self) -> None:
        self.tags = [
            TaskTag.get_or_create(name)
            for name in [
                t.strip(' #').lower()
                for t in hashtag_re.findall(self.raw_text)
            ]
        ]


db.generate_mapping(create_tables=True)
