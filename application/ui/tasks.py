from typing import *
from tornado.web import UIModule
from application.models import Task


class TaskList(UIModule):
    def render(self, tasks: List[Task]):
        return self.render_string(
            'modules/task_list.html',
            tasks=tasks
        )
