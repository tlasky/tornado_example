from wtforms import Form
from wtforms.fields import StringField, PasswordField, SubmitField, TextAreaField
from wtforms.fields.html5 import DateTimeLocalField
from wtforms.validators import DataRequired, Length, Optional

date_format = '%d/%m/%Y'


class LoginForm(Form):
    username = StringField('Username', validators=[DataRequired(), Length(3, 15)])
    password = PasswordField('Password', validators=[DataRequired(), Length(3, 20)])
    submit = SubmitField('Login')


class TaskForm(Form):
    text = TextAreaField('Task text', validators=[DataRequired()])
    valid_from = DateTimeLocalField('Valid from', validators=[Optional()], format=date_format)
    valid_to = DateTimeLocalField('Valid to', validators=[Optional()], format=date_format)
