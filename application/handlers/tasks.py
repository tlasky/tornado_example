from application.handlers import BaseHandler
from tornado.web import authenticated

from application.models import *
from application.forms import *


class DashboardHandler(BaseHandler):
    @db_session
    @authenticated
    def get(self):
        self.render(
            'dashboard.html',
            tags=self.current_user.tags,
            tasks=self.current_user.tasks.order_by(Task.created_at)
        )


class TaskAddHandler(BaseHandler):
    @authenticated
    async def get(self):
        return await self.render(
            'task.html',
            action='Add',
            form=TaskForm(**self.get_all_arguments())
        )

    @db_session
    @authenticated
    def post(self):
        form = TaskForm(**self.get_all_arguments())
        if form.validate():
            Task(
                raw_text=form.text.data,
                valid_from=form.valid_from.data or None,
                valid_to=form.valid_to.data or None,
                owner=self.current_user
            )
            db.commit()
            self.redirect('/dashboard')
            return
        self.flash_form_errors(form)
        return self.render(
            'task.html',
            action='Add',
            form=form
        )


class TaskEditHandler(BaseHandler):
    def get_task(self, uid: int) -> Union[None, Task]:
        task = Task.select(
            lambda t:
            t.id == uid and
            t.owner is self.current_user
        ).first()
        if not task:
            self.set_status(404)
            self.flash('Task was not found!', 'danger')
            self.redirect('/dashboard')
        return task

    @db_session
    @authenticated
    def get(self, uid: int):
        if not (task := self.get_task(uid)):
            return
        return self.render(
            'task.html',
            action='Edit',
            form=TaskForm(
                text=task.text,
                **task.to_dict()
            )
        )

    @db_session
    @authenticated
    def post(self, uid: int):
        form = TaskForm(**self.get_all_arguments())
        if form.validate():
            if not (task := self.get_task(uid)):
                return
            if form.text.data.strip():
                task.text = form.text.data
                task.valid_from = form.valid_from.data or None
                task.valid_to = form.valid_to.data or None
            else:
                task.delete()
            db.commit()
        self.flash_form_errors(form)
        return self.render(
            'task.html',
            action='Edit',
            form=form,
        )


class TaskSearchHandler(BaseHandler):
    @db_session
    @authenticated
    def get(self):
        q = self.get_argument('q', str())
        tag_names = [
            name.strip(' #').lower()
            for name in q.split(' ')
            if name.strip()
        ]
        tasks = Task.select(
            lambda task:
            task.owner is self.current_user and
            (
                tag.name in tag_names
                for tag in task.tags
            )
        ) if tag_names else self.current_user.tasks
        self.render(
            'task_search.html',
            tasks=tasks,
            q=q
        )
