from application.handlers import BaseHandler
from tornado.web import authenticated

from application.models import *
from application.forms import *


class IndexHandler(BaseHandler):
    async def get(self):
        if self.current_user:
            self.redirect('/dashboard')
            return
        await self.render('index.html')


class LoginHandler(BaseHandler):
    async def get(self):
        if self.current_user:
            self.redirect(self.get_default_url())
            return
        await self.render(
            'login.html',
            form=LoginForm(**self.get_all_arguments())
        )

    @db_session
    async def post(self):
        form = LoginForm(**self.get_all_arguments())
        if form.validate():
            user = User.get(username=self.get_argument('username'))
            if user and user.check_password(self.get_argument('password')):
                self.set_secure_cookie(self.user_cookie_name, str(user.id))
                self.flash('You are successfully logged in!', 'success')
                self.redirect(self.get_default_url())
                return
            else:
                self.flash('Bad username or password!', 'warning')
        self.flash_form_errors(form)
        await self.render(
            'login.html',
            form=form
        )


class LogoutHandler(BaseHandler):
    @authenticated
    async def get(self):
        self.clear_cookie(self.user_cookie_name)
        self.flash('You have been logged out!', 'success')
        self.redirect(self.get_login_url())
