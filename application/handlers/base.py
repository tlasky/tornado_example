from typing import *
from wtforms import Form
from tornado.web import *
from tornado.escape import *
from application.models import *


class BaseHandler(RequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._flashes: List[Tuple[str, str]] = list()
        self.flashes_cookie_name = 'flashes'
        self.user_cookie_name = 'user'

    def data_received(self, chunk: bytes):
        """
        RequestHandler is ABC, so I need to "implement" this.
        """
        pass

    def _get_merged_flashes(self) -> List[Tuple[str, str]]:
        """
        To get flashes from same and other handler, the flashes need to be sat as variable and cookie.
        Then the variable and cookie need to be merged.
        """
        cookie_flashes_str = self.get_cookie(self.flashes_cookie_name, None)
        cookie_flashes = list() if not cookie_flashes_str else [
            tuple(flash)
            for flash in json_decode(url_unescape(cookie_flashes_str))
        ]
        return list(dict.fromkeys(self._flashes + cookie_flashes))

    def flash(self, message: str, category: str = 'info') -> None:
        """
        Flashing messages as Flask.
        """
        self._flashes.append((category, message))
        self._flashes = self._get_merged_flashes()
        self.set_cookie(self.flashes_cookie_name, url_escape(json_encode(self._flashes)))

    def flash_form_errors(self, form: Form, category: str = 'warning') -> None:
        """
        Flash form validation errors.
        """
        for field_name, field_errors in form.errors.items():
            field = getattr(form, field_name)
            message = ' '.join(field_errors)
            self.flash(f'{field.label.text}: {message}', category)

    def get_flashes(self) -> List[Tuple[str, str]]:
        """
        Getting flashed messages in templates as Flask.
        """
        flashes = self._get_merged_flashes()
        self._flashes = list()
        self.clear_cookie(self.flashes_cookie_name)
        return flashes

    def get_template_namespace(self) -> Dict[str, Any]:
        """
        Modifying templates context / namespace.
        """
        name_space = super().get_template_namespace()
        name_space.update(dict(
            get_flashes=self.get_flashes
        ))
        return name_space

    def get_current_user(self) -> Union[None, User]:
        """
        Getting current user from database according to cookies.
        No @db_session decorator, because db_session is not required in all cases.
        """
        if uid := self.get_secure_cookie(self.user_cookie_name):
            with db_session:
                user = User.get(id=int(uid))
                if not user:
                    self.clear_cookie(self.user_cookie_name)
                return user

    def get_default_url(self) -> str:
        """
        Default url shortcut.
        """
        return self.get_argument('next', '/')

    def get_all_arguments(self) -> Dict[str, Any]:
        """
        Get all arguments as dict.
        Used for wtforms (and also useful for debug reasons).
        """
        return {
            key: self.get_argument(key)
            for key in self.request.arguments.keys()
        }
