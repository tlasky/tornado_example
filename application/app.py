import config
from tornado.web import Application
from application.handlers import *
from tornado.web import StaticFileHandler
import application.ui as ui_modules

settings = config.__dict__
settings['ui_modules'] = ui_modules

application = Application(
    [
        # Common
        (r'/', IndexHandler),

        # Auth
        (config.login_url, LoginHandler),
        (r'/logout', LogoutHandler),

        # Tasks
        (r'/dashboard', DashboardHandler),
        (r'/task/add', TaskAddHandler),
        (r'/task/edit/([0-9]+)', TaskEditHandler),
        (r'/task/search', TaskSearchHandler),

        # Needs to be at the end.
        (r'/(.*)', StaticFileHandler, dict(path=config.static_path)),
    ],
    **settings
)
