from config import port
from application import application
from tornado.ioloop import IOLoop

try:
    print(f'\t* Running on port {port}')
    application.listen(port)
    IOLoop.current().start()  # Starting server
except KeyboardInterrupt:
    pass
