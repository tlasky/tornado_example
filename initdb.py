from application import db, db_session, User, Task

"""
Creating User[1] if not exists.
"""
with db_session:
    user = User.get(id=1)
    if not user:
        user = User(
            username='tlasky',
            password_hash=User.generate_password_hash('123')
        )
        db.commit()

    if not user.tasks:
        task = Task(
            raw_text='Hello #world!',
            owner=user
        )
        task.update()
        db.commit()
