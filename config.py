import os
import secrets

base_path = os.path.dirname(os.path.abspath(__file__))

template_path = os.path.join(base_path, 'templates')
static_path = os.path.join(base_path, 'static')

secret_path = os.path.join(base_path, 'secret_key.txt')
database_path = os.path.join(base_path, 'database.sqlite')

# Generating secret key
if not os.path.isfile(secret_path):
    with open(secret_path, 'w') as file:
        file.write(secrets.token_urlsafe())

port = int(os.environ.get('PORT', 5000))
debug = True
autoreload = debug
compiled_template_cache = False
cookie_secret = open(secret_path, 'r').read()

pony = dict(
    provider='sqlite',
    filename=database_path,
    create_db=True
)

login_url = r'/login'

settings = locals().copy()
